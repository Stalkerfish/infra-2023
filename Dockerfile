# Building Go Backend
FROM golang:latest AS builder

WORKDIR /app

# Copy the Go application source code into the container
COPY . .

# Build the Go application with minimal dependencies
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o infra0 .


# Create a lightweight image
FROM alpine:latest

# Copy the Go application binary from the builder
COPY --from=builder /app/infra0 /usr/local/bin/infra0

WORKDIR /app

# Open port for connections
EXPOSE 2121
EXPOSE 6379

# Command to run the Go application (assumes it connects to Redis at "localhost:6379")
CMD ["infra0"]

Infra0 Project
===

![downloads](https://img.shields.io/github/downloads/atom/atom/total.svg)
![build](https://img.shields.io/appveyor/ci/:user/:repo.svg)
![chat](https://img.shields.io/discord/:serverId.svg)

Welcome to the project wiki for our containerized Go backend with Docker Compose and Redis. This wiki is designed to provide detailed documentation and guidance for setting up and using our application.

## Table of Contents
1. [Getting Started](#getting-started)
2. [Usage](#usage)
3. [Troubleshooting](#troubleshooting)

## Getting Started

Before you begin, make sure you have the following prerequisites installed:
- Docker: [Install Docker](https://docs.docker.com/get-docker/)
- Docker Compose: [Install Docker Compose](https://docs.docker.com/compose/install/)
- Git: [Install Git](https://git-scm.com/downloads)
- curl: [Install curl](https://github.com/curl/curl)


Download `dev-locally.sh` to your local machine:

```bash
wget https://gitlab.com/Stalkerfish/infra-2023/-/raw/main/dev-locally.sh
```


Usage:
```bash
PORT=<number> sh dev-locally.sh
```

It will build the backend image and compose the application using the desired port.

After the container execution is done, there is a dialog asking if Marcos want to upload the image to GitLab Registry

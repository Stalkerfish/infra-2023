#!/bin/sh

# Grab the localhost port
colectPort() {

 if [ -z "$PORT" ]; then
   echo "Error: PORT environment variable is not set"
   exit 1
 fi

 if ! [[ "$PORT" =~ ^[0-9]+$ ]]; then
   echo "Error: PORT is not a valid integer."
   exit 1
 fi

 if ((PORT < 1 || PORT > 65535)); then
   echo "Error: PORT is not within the valid range for localhost (1-65535)."
   exit 1
 fi

 rm --force .env

 # docker-compose will read .env file for building images
 echo "port=$PORT" >> .env
 echo "REDISPORT=6379" >> .env

}

# Get the project files
getProject() {
  DIR=infra-2023
  ORIGIN=https://gitlab.com/Stalkerfish/infra-2023.git

  if [ -d $DIR ]; then
    cd $DIR
    git pull
  else
    mkdir $DIR
    cd $DIR
    git clone $ORIGIN .
  fi
}

run() {
  (trap - INT; exec sudo docker compose up)

}

knowIfWant() {
  echo "Do you want to upload this image to registry? (1/2)"

  select yn in "Yes" "No"; do
    case $yn in
      Yes ) uploadRegistry ;;
      No ) exit;;
    esac
  done
}

uploadRegistry() { 

  loginRegistry() {
    GITLAB_REGISTRY=registry.gitlab.com
    echo "Login into your GitLab Registry account:"
    sleep 3
    sudo docker login $GITLAB_REGISTRY

  }

  buildRegistryImage() {
    echo "Building your image for registry"
    
    echo "Enter your project name (like on GitLab but lowcaps and '-' replaces empty spaces): "
    read name
    PROJECT_NAME=$name
    
    echo "Enter your Gitlab Username (also lowcaps): "
    read username
    USER_NAME=$username

    sudo docker tag infra0:latest $GITLAB_REGISTRY/$USER_NAME/$PROJECT_NAME

  }

  dockerPush() {
    echo "Pushing..."
    sudo docker push $GITLAB_REGISTRY/$USER_NAME/$PROJECT_NAME

  }

  cleanLocalImages() {
    echo "Do you want to clean local images? (1/2)"

    select yn in "Yes" "No"; do
      case $yn in
        Yes ) sudo docker rmi $GITLAB_REGISTRY/$USER_NAME/$PROJECT_NAME:latest; exit;;
        No ) exit;;
      esac
    done

  }
  
  loginRegistry
  buildRegistryImage
  dockerPush
  cleanLocalImages

}

trap '' INT

getProject
colectPort
run
knowIfWant
